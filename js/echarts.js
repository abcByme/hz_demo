 var html = document.getElementsByTagName("html")[0];
 var width = html.clientWidth;
 var tooltipFontSize = 14 / 1280 * width;
 var fontSize = 10 / 1280 * width;
 var legendFontSize = 10 / 1280 * width;
 var labelFontSize = 10 / 1280 * width;
 var itemHeight = 8 / 1280 * width;
 var itemWidth = 10 / 1280 * width;
 // 基于准备好的dom，初始化echarts实例
 //-------------------------------------园区类型分布
 var myChart1 = echarts.init(document.getElementById('echarts1'));

 /*window.onresize = Charts.curCharts.resize;*/

 myChart1.setOption({
     tooltip: {
         confine: true,
         trigger: 'item',
         formatter: "{a} <br/>{b}: {c} ({d}%)",

     },
     legend: {
         orient: 'vertical',
         x: 'right',
         y: 'center',
         itemWidth: itemWidth,
         itemHeight: itemHeight,
         align: 'left',

         data: ['工业园', '科技园', '电商园', ],
         textStyle: {
             color: '#fff'
         }
     },
     series: [{
         name: '项目进展类型占比',
         type: 'pie',
         center: ["35%", "50%"],
         radius: ['38%', '58%'],
         color: ['#69E2F8', '#27BFAA', '#4188FF'],
         label: {
             normal: {
                 formatter: '{b}\n{d}%',
                 show: false,
             },
         },
         data: [{
                 value: '20',
                 name: '工业园'
             }, {
                 value: '45',
                 name: '科技园'
             }, {
                 value: '55',
                 name: '电商园'
             },

         ]
     }]
 });

 //-------------------------------------园区智能化情况
 var myChart2 = echarts.init(document.getElementById('echarts2'));
 myChart2.setOption(
     option = {
         // title: {
         //     text: '世界人口总量',
         //     subtext: '数据来自网络'
         // },
         color: ['#3398DB'],
         tooltip: {
             trigger: 'axis',
             axisPointer: {
                 type: 'shadow'
             },
             textStyle: {
                 color: '#fff',
                 fontSize: tooltipFontSize,
             },
         },
         legend: {
             itemWidth: itemWidth,
             itemHeight: itemHeight,
             selectedMode: 'single',
             // data: ['2018年', '2019年'],
             textStyle: {
                 fontSize: legendFontSize
             }
         },
         grid: {
             left: '0%',
             right: '0%',
             bottom: '0%',
             top: '0%',
             containLabel: true
         },
         xAxis: {
             type: 'value',
             boundaryGap: [0, 0.01],
             axisLine: {
                 lineStyle: {
                     color: '#fff',
                 }
             }
         },
         yAxis: {
             type: 'category',
             data: ['环境监测', '智慧安防', '智能门禁', '智能停车', '智慧路灯'],
             axisLine: {
                 lineStyle: {
                     color: '#fff',
                 }
             },
             axisLabel: {
                 show: true,
                 textStyle: {
                     // color: '#9E9EA8', //更改坐标轴文字颜色
                     // fontSize: 10 //更改坐标轴文字大小
                 },
                 // interval: 0,
                 // rotate: 30
             },
         },
         textStyle: {
             color: '#fff'
         },
         series: [{
             // name: '2019年',
             type: 'bar',
             data: [11, 8, 11, 12, 5]
         }],
         itemStyle: {
             normal: {

                 color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                     offset: 0,
                     color: 'rgba(17, 168,171, 1)'
                 }, {
                     offset: 1,
                     color: 'rgba(17, 168,171, 0.1)'
                 }]),
                 shadowColor: 'rgba(0, 0, 0, 0.1)',
                 shadowBlur: 10
             }
         }
     }
 );

 //-------------------------------------园区入驻产业类型分布
 var myChart3 = echarts.init(document.getElementById('echarts3'));

 /*window.onresize = Charts.curCharts.resize;*/

 myChart3.setOption({
     tooltip: {
         confine: true,
         trigger: 'item',
         formatter: "{a} <br/>{b}: {c} ({d}%)",

     },
     legend: {
         orient: 'vertical',
         x: 'right',
         y: 'center',
         itemWidth: itemWidth,
         itemHeight: itemHeight,
         align: 'left',
         data: ['住宿、餐饮业', '金融业', '交通运输、仓储', '批发、零售业'],
         textStyle: {
             color: '#fff'
         }
     },
     series: [{
         name: '项目进展类型占比',
         type: 'pie',
         center: ["25%", "50%"],
         radius: ['38%', '58%'],
         color: ['#4188FF', '#FFB43A', '#BD384D', '#10F5F5'],
         label: {
             normal: {
                 formatter: '{b}\n{d}%',
                 show: false,
             },
         },
         data: [{
                 value: '20',
                 name: '住宿、餐饮业'
             }, {
                 value: '45',
                 name: '金融业'
             }, {
                 value: '55',
                 name: '交通运输、仓储'
             }, {
                 value: '50',
                 name: '批发、零售业'
             }

         ]
     }]
 });


 //-------------------------------------园区入驻企业数
 var myChart4 = echarts.init(document.getElementById('echarts4'));

 /*window.onresize = Charts.curCharts.resize;*/

 myChart4.setOption({
     color: ['#3398DB'],
     tooltip: {
         trigger: 'axis',
         axisPointer: { // 坐标轴指示器，坐标轴触发有效
             type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
         },
         confine: true,
     },
     legend: {
         itemWidth: itemWidth,
         itemHeight: itemHeight,
     },
     grid: {
         left: '0%',
         right: '0%',
         bottom: '0%',
         top: '0%',
         containLabel: true
     },
     xAxis: [{
         type: 'category',
         data: ['汇富春天', '友地', '飞跃', '太平塘', '义乌'],
         axisTick: {
             alignWithLabel: true
         },
         axisLine: {
             lineStyle: {
                 color: '#fff',
             }
         },
         axisLabel: {
             show: true,
             textStyle: {
                 color: '#9E9EA8', //更改坐标轴文字颜色
                 fontSize: 12 //更改坐标轴文字大小
             },
             interval: 0,
             rotate: 40
         },
         axisTick: {
             show: false
         },
         axisLine: {
             lineStyle: {
                 color: '#9E9EA8' //更改坐标轴颜色
             }
         }

     }],
     // yAxis: [{
     //     type: 'value',
     //     axisLine: {
     //         lineStyle: {
     //             color: '#fff',
     //         }
     //     },
     //     splitLine: {
     //         show: true,
     //         interval: 'auto',
     //         lineStyle: {
     //             color: ['#333'],
     //         }
     //     }
     // }],
     yAxis: {
         min: 0,
         max: 80,
         // splitNumber: 20,
         axisLabel: {
             // formatter: function(value) {
             //     var texts = [];
             //     if (value == 0) {
             //         texts.push('woo');
             //     } else if (value <= 1) {
             //         texts.push('好');
             //     } else if (value <= 2) {
             //         texts.push('很好');
             //     } else if (value <= 3) {
             //         texts.push('非常好');
             //     } else {
             //         texts.push('完美');
             //     }
             //     return texts;

             // }
         },
         axisLine: {
             lineStyle: {
                 color: '#fff',
             }
         },
     },
     series: [{
         // name: '家',
         type: 'bar',
         barWidth: '40%',
         data: [40, 40, 30, 20, 60]
     }]
 });


 //避免加载失败
 setTimeout(function() {
     $('.echarts-box').show();
     myChart1.resize();
     myChart2.resize();
     myChart3.resize();
     myChart4.resize();
 }, 1000);
 window.onresize = function() {
     myChart1.resize();
     myChart2.resize();
     myChart3.resize();
     myChart4.resize();
 };
