$(function() { //dom加载后执行
    mapChart('mapChart');
    //窗口变化重新加载
    window.addEventListener("resize", function() {
        mapChart('mapChart');
    });
});

/**
 * 根据Json里的数据构造Echarts地图所需要的数据
 * @param {} mapJson 
 */
var uploadedDataURL = "../asset/json/chn.geo.json";

function initMapData(mapJson) {
    var mapData = [];
    for (var i = 0; i < mapJson.features.length; i++) {
        mapData.push({
            name: mapJson.features[i].properties.name,
        })
    };
    return mapData;
};
/**
 * 返回上一级地图
 */
function back() {
    if (mapStack.length != 0) { //如果有上级目录则执行
        var map = mapStack.pop();
        $.get('./asset/json/map/' + map.mapId + '.json', function(mapJson) {
            registerAndsetOption(myChart, map.mapId, map.mapName, mapJson, false)
                //返回上一级后，父级的ID、Name随之改变
            parentId = map.mapId;
            parentName = map.mapName;
        })
    }
};
/**
 * Echarts地图
 */

//中国地图（第一级地图）的ID、Name、Json数据-默认
// var chinaId = 100000;
var chinaId = 330000;
// var chinaName = 'china'
var chinaName = '浙江'
var chinaJson = null;
//记录父级ID、Name
var mapStack = [];
var parentId = null;
var parentName = null;
//Echarts地图全局变量，主要是在返回上级地图的方法中会用到
var myChart = null;

function mapChart(divid) {
    $.get('./asset/json/map/' + chinaId + '.json', function(mapJson) {
        chinaJson = mapJson;
        myChart = echarts.init(document.getElementById(divid));
        registerAndsetOption(myChart, chinaId, chinaName, mapJson, false)
        parentId = chinaId;
        parentName = 'china'
        myChart.on('click', function(param) {
            var cityId = cityMap[param.name]
            if (cityId) { //代表有下级地图
                $.get('./asset/json/map/' + cityId + '.json', function(mapJson) {
                    registerAndsetOption(myChart, cityId, param.name, mapJson, true)
                })
            } else {
                //没有下级地图，回到一级中国地图，并将mapStack清空
                registerAndsetOption(myChart, chinaId, chinaName, chinaJson, false)
                mapStack = []
                parentId = chinaId;
                parentName = chinaName;
            }
        });
    })
};
var geoCoordMap = { //对应显示圆圈圈
    '杭州': [120.14422, 30.296248],
    '湖州': [120.106639, 30.869758],
    '嘉兴': [120.769303, 30.769686],
    '宁波': [121.543463, 29.868652],
    '金华': [119.653452, 29.078394],
    '丽水': [119.930653, 28.455006],
    '衢州': [118.865676, 28.975667],
    '绍兴': [120.593428, 29.995678],
    '台州': [121.427571, 28.662392],
    '温州': [120.705725, 28.001095],
    '舟山': [122.118184, 30.02662],
    '兰州': [103.838224, 36.060233],
    "渭南": [109.466575, 34.500479],
    "上海市": [121.4648, 31.2891],
    "河北省": [114.502461, 38.045474],
    "山西省": [112.549248, 37.857014],
    "内蒙古": [111.670801, 40.818311],
    "辽宁省": [123.429096, 41.796767],
    "吉林省": [125.3245, 43.886841],
    "黑龙江省": [126.642464, 45.756967],
    "江苏省": [118.767413, 32.041544],
    "浙江省": [120.153576, 30.287459],
    "安徽省": [117.283042, 31.86119],
    "福建省": [119.306239, 26.075302],
    "江西省": [115.892151, 28.676493],
    "山东省": [117.000923, 36.675807],
    "河南省": [113.665412, 34.757975],
    "湖北省": [114.298572, 30.584355],
    "湖南省": [112.982279, 28.19409],
    "广东省": [113.280637, 23.125178],
    "广西省": [108.320004, 22.82402],
    "海南省": [110.33119, 20.031971],
    "四川省": [104.065735, 30.659462],
    "贵州省": [106.713478, 26.578343],
    "云南省": [102.712251, 25.040609],
    "西藏": [91.132212, 29.660361],
    "陕西省": [108.948024, 34.263161],
    "甘肃省": [103.823557, 36.058039],
    "青海省": [101.778916, 36.623178],
    "宁夏": [106.278179, 38.46637],
    "新疆": [87.617733, 43.792818],
    "北京市": [116.405285, 39.904989],
    "天津市": [117.190182, 39.125596],
    "上海市": [121.472644, 31.231706],
    "重庆市": [106.504962, 29.533155],
    "香港": [114.173355, 22.320048],
    "澳门": [113.54909, 22.198951],
    "童装城": [119.858649, 30.952596],
    "砂洗城": [119.982831, 30.847985],
    "中节能": [119.863248, 30.835582]
};

var data1 = [
    { name: '杭州', value: 190 },
    { name: '湖州', value: 190 },
    { name: '嘉兴', value: 190 },
    { name: '宁波', value: 190 },
    { name: '金华', value: 190 },
    { name: '丽水', value: 190 },
    { name: '绍兴', value: 190 },
    { name: '台州', value: 190 },
    { name: '温州', value: 190 },
    { name: '舟山', value: 190 },
    { name: '衢州', value: 190 },
    { name: '童装城', value: '' },
    { name: '砂洗城', value: '' },
    { name: '中节能', value: '' },

];
var planePath = 'path://M1705.06,1318.313v-89.254l-319.9-221.799l0.073-208.063c0.521-84.662-26.629-121.796-63.961-121.491c-37.332-0.305-64.482,36.829-63.961,121.491l0.073,208.063l-319.9,221.799v89.254l330.343-157.288l12.238,241.308l-134.449,92.931l0.531,42.034l175.125-42.917l175.125,42.917l0.531-42.034l-134.449-92.931l12.238-241.308L1705.06,1318.313z';
var moveLine = {
    'normal': [{
            "fromName": "杭州",
            "toName": "湖州",
            'coords': [
                [120.14422, 30.296248],
                [120.106639, 30.869758]
            ]
        }, {
            "fromName": "杭州",
            "toName": "嘉兴",
            'coords': [
                [120.14422, 30.296248],
                [120.769303, 30.769686]
            ]
        }, {
            "fromName": "杭州",
            "toName": "宁波",
            'coords': [
                [120.14422, 30.296248],
                [121.543463, 29.868652]
            ]
        }, {
            "fromName": "杭州",
            "toName": "金华",
            'coords': [
                [120.14422, 30.296248],
                [119.653452, 29.078394]
            ]
        }, {
            "fromName": "杭州",
            "toName": "丽水",
            'coords': [
                [120.14422, 30.296248],
                [119.930653, 28.455006]
            ]
        }, {
            "fromName": "杭州",
            "toName": "衢州",
            'coords': [
                [120.14422, 30.296248],
                [118.865676, 28.975667]
            ]
        }, {
            "fromName": "杭州",
            "toName": "绍兴",
            'coords': [
                [120.14422, 30.296248],
                [120.593428, 29.995678]
            ]
        }, {
            "fromName": "杭州",
            "toName": "台州",
            'coords': [
                [120.14422, 30.296248],
                [121.427571, 28.662392]
            ]
        }, {
            "fromName": "杭州",
            "toName": "温州",
            'coords': [
                [120.14422, 30.296248],
                [120.705725, 28.001095]
            ]
        },

        {
            "fromName": "杭州",
            "toName": "舟山",
            'coords': [
                [120.14422, 30.296248],
                [122.118184, 30.02662]
            ]
        },
    ],
    'warning': [

    ]
};

// function convertData(data) {
//     var res = [];
//     for (var i = 0; i < data.length; i++) {
//         var dataItem = data[i];
//         var fromCoord = geoCoordMap[dataItem[0].name];
//         var toCoord = geoCoordMap[dataItem[1].name];
//         if (fromCoord && toCoord) {
//             res.push([{
//                 coord: fromCoord,
//                 value: dataItem[0].value
//             }, {
//                 coord: toCoord,
//             }]);
//         }
//     }
//     return res;
// };
var convertData = function(data) {
    var res = [];
    for (var i = 0; i < data.length; i++) {
        var geoCoord = geoCoordMap[data[i].name];
        if (geoCoord) {
            res.push({
                name: data[i].name,
                value: geoCoord.concat(data[i].value)
            });
        }
    }
    return res;
};
// ['杭州', BJData].forEach(function(item, i) {
//     // console.log(item);
// });
/**
 * 
 * @param {*} myChart 
 * @param {*} id        省市县Id
 * @param {*} name      省市县名称
 * @param {*} mapJson   地图Json数据
 * @param {*} flag      是否往mapStack里添加parentId，parentName
 */
var series = [];

function registerAndsetOption(myChart, id, name, mapJson, flag) {
    echarts.registerMap(name, mapJson);
    if (flag) { //下一级不显示一些东西

        var option = {};
        option = {
            title: {
                top: 20,
                text: '',
                subtext: '',
                x: 'center',
                textStyle: {
                    color: '#ccc'
                }
            },
            // tooltip: {
            //     formatter: function(params) {
            //         var info = '<p style="font-size:18px">' + params.name + '</p><p style="font-size:14px">这里可以写一些，想展示在页面上的别的信息</p>'
            //         return info;
            //     },
            //     backgroundColor: "#ff7f50", //提示标签背景颜色 
            //     textStyle: { color: "#fff" } //提示标签字体颜色 
            // },
            visualMap: {
                show: false,
                min: 0,
                max: 500,
                left: 'left',
                top: 'bottom',
                text: ['高', '低'], // 文本，默认为数值文本
                calculable: true,
                seriesIndex: [1],
                inRange: {
                    // color: ['#3B5077', '#031525'] // 蓝黑
                    // color: ['#ffc0cb', '#800080'] // 红紫
                    // color: ['#3C3B3F', '#605C3C'] // 黑绿
                    //color: ['#0f0c29', '#302b63', '#24243e'] // 黑紫黑
                    //color: ['#23074d', '#cc5333'] // 紫红
                    // color: ['#00467F', '#A5CC82'] // 蓝绿
                    // color: ['#1488CC', '#2B32B2'] // 浅蓝
                    // color: ['#00467F', '#A5CC82'] // 蓝绿
                    // color: ['#00467F', '#A5CC82'] // 蓝绿
                    // color: ['#00467F', '#A5CC82'] // 蓝绿
                    // color: ['#00467F', '#A5CC82'] // 蓝绿

                }
            },
            geo: {
                show: true,
                map: name,
                zoom: 1.2,
                roam: false, //是否允许缩放
                label: { //地图区块名字
                    normal: {
                        show: true,
                        textStyle: {
                            color: '#fff'
                        }
                    },
                    emphasis: {
                        textStyle: {
                            color: '#fff'
                        }
                    }
                },
                itemStyle: { //区域颜色
                    normal: {
                        areaColor: 'transparent',
                        borderColor: '#3fdaff',
                        borderWidth: 2,
                        shadowColor: 'rgba(63, 218, 255, 0.5)',
                        shadowBlur: 30
                    },
                    emphasis: {
                        areaColor: '#2B91B7',
                    }
                }
            },
            series: [{
                type: 'map',
                map: name,
                geoIndex: 0,
                zoom: 1.2,
                aspectScale: 0.75, //长宽比
                showLegendSymbol: false, // 存在legend时显示
                label: {
                    normal: {
                        show: false
                    },
                    emphasis: {
                        show: false,
                        textStyle: {
                            color: '#fff'
                        }
                    }
                },
                itemStyle: {
                    normal: {
                        areaColor: '#031525',
                        borderColor: '#FFFFFF',
                    },
                    emphasis: {
                        areaColor: '#2B91B7'
                    }
                },
                animation: false,
                data: initMapData(mapJson)
            }, { //圈圈发光
                name: name,
                type: 'effectScatter',
                coordinateSystem: 'geo',
                data: convertData(data1.sort(function(a, b) {
                    return b.value - a.value;
                }).slice(11, 14)),
                // data: convertData(data1.sort(function(a, b) {
                //     return b.value - a.value;
                // })),
                // symbolSize: function(val) {
                //     // console.log('val',val[2] / 10) 19
                //     return val[2] / 10;
                // },
                symbolSize: 20,
                showEffectOn: 'render',
                rippleEffect: {
                    brushType: 'stroke'
                },
                hoverAnimation: true,
                hoverAnimation: false,
                label: {
                    normal: {
                        // formatter: '{b}',
                        formatter: function(params) {
                            var v = params.data.value;
                            var name = params.data.name;
                            var str = name + v[2];
                            return str

                        },
                        position: 'bottom',
                        show: true,
                        // color: '#01F1F3'
                        color: '#fff'
                    }
                },
                itemStyle: {
                    normal: {
                        color: '#F4E925',
                        // color:'#01F1F3',
                        // color:'#414767',
                        shadowBlur: 10,
                        shadowColor: '#05C3F9'
                    }
                },
                zlevel: 1
            }]
        };
        mapStack.push({
            mapId: parentId,
            mapName: parentName
        });
        parentId = id;
        parentName = name;
        $('.sxc-box').show();
        $('.tzc-box').show();
        $('.zjn-box').show();
    } else { //默认进入设置-第一级
        var option = {};
        option = {
            title: {
                top: 20,
                text: '',
                subtext: '',
                x: 'center',
                textStyle: {
                    color: '#ccc'
                }
            },
            // tooltip: {
            //     trigger: 'item',
            //     formatter: function(params) {
            //         if (typeof(params.value)[2] == "undefined") {
            //             return params.name + ' : ' + params.value;
            //         } else {
            //             return params.name + ' : ' + params.value[2];
            //         }
            //     }
            // },
            // legend: {
            //     orient: 'vertical',
            //     y: 'bottom',
            //     x: 'right',
            //     data: ['pm2.5'],
            //     textStyle: {
            //         color: '#fff'
            //     }
            // },
            visualMap: {
                show: false,
                min: 0,
                max: 500,
                left: 'left',
                top: 'bottom',
                text: ['高', '低'], // 文本，默认为数值文本
                calculable: true,
                seriesIndex: [1],
                inRange: {
                    // color: ['#3B5077', '#031525'] // 蓝黑
                    // color: ['#ffc0cb', '#800080'] // 红紫
                    // color: ['#3C3B3F', '#605C3C'] // 黑绿
                    //color: ['#0f0c29', '#302b63', '#24243e'] // 黑紫黑
                    //color: ['#23074d', '#cc5333'] // 紫红
                    // color: ['#00467F', '#A5CC82'] // 蓝绿
                    // color: ['#1488CC', '#2B32B2'] // 浅蓝
                    // color: ['#00467F', '#A5CC82'] // 蓝绿
                    // color: ['#00467F', '#A5CC82'] // 蓝绿
                    // color: ['#00467F', '#A5CC82'] // 蓝绿
                    // color: ['#00467F', '#A5CC82'] // 蓝绿

                }
            },
            // toolbox: { //悬浮窗
            //     show: true,
            //     orient: 'vertical',
            //     left: 'right',
            //     top: 'center',
            //     feature: {
            //             dataView: {readOnly: false},
            //             restore: {},
            //             saveAsImage: {}
            //             }
            // },
            geo: {
                show: true,
                map: name,
                zoom: 1.2,
                roam: false, //是否允许缩放
                label: { //地图区块名字
                    normal: {
                        show: false,
                        textStyle: {
                            color: '#fff'
                        }
                    },
                    emphasis: {
                        textStyle: {
                            color: '#fff'
                        }
                    }
                },
                itemStyle: { //区域颜色
                    normal: {
                        areaColor: 'transparent',
                        borderColor: '#3fdaff',
                        borderWidth: 2,
                        shadowColor: 'rgba(63, 218, 255, 0.5)',
                        shadowBlur: 30
                    },
                    emphasis: {
                        areaColor: '#2B91B7',
                    }
                }
            },
            series: [
                // { //小圆圈圈
                //         name: name,
                //         type: 'scatter',
                //         coordinateSystem: 'geo',
                //         zoom: 1.2,
                //         roam: false, //是否允许缩放
                //         data: convertData(data1),
                //         symbolSize: function(val) {
                //             return val[2] / 10;
                //         },
                //         // symbolSize: 50,
                //         label: {
                //             normal: {
                //                 // formatter: '{b}',
                //                 // position: 'left',
                //                 // show: true
                //             },
                //             emphasis: {
                //                 show: true
                //             }
                //         },
                //         itemStyle: {
                //             normal: {
                //                 color: '#F4E925'
                //             }
                //         }
                //     }, 
                {
                    type: 'map',
                    map: name,
                    roam: false,
                    zoom: 1.2,
                    geoIndex: 0,
                    // aspectScale: 0.75, //长宽比
                    showLegendSymbol: false, // 存在legend时显示
                    label: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: false,
                            textStyle: {
                                color: '#fff'
                            }
                        }
                    },
                    itemStyle: {
                        normal: {
                            areaColor: '#031525',
                            borderColor: '#FFFFFF',
                        },
                        emphasis: {
                            areaColor: '#2B91B7'
                        }
                    },
                    animation: false,
                    data: initMapData(mapJson)
                }, { //圈圈发光
                    name: name,
                    type: 'effectScatter',
                    coordinateSystem: 'geo',
                    data: convertData(data1.sort(function(a, b) {
                        return b.value - a.value;
                    }).slice(0, 11)),
                    // data: convertData(data1.sort(function(a, b) {
                    //     return b.value - a.value;
                    // })),
                    // symbolSize: function(val) {
                    //     // console.log('val',val[2] / 10) 19
                    //     return val[2] / 10;
                    // },
                    symbolSize: 20,
                    showEffectOn: 'render',
                    rippleEffect: {
                        brushType: 'stroke'
                    },
                    hoverAnimation: true,
                    hoverAnimation: false,
                    label: {
                        normal: {
                            // formatter: '{b}',
                            formatter: function(params) {
                                var v = params.data.value;
                                var name = params.data.name;
                                var str = name + v[2];
                                return str

                            },
                            position: 'bottom',
                            show: true,
                            // color: '#01F1F3'
                            color: '#fff'
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#F4E925',
                            // color:'#01F1F3',
                            // color:'#414767',
                            shadowBlur: 10,
                            shadowColor: '#05C3F9'
                        }
                    },
                    zlevel: 1
                },
                //线路-
                {
                    name: '线路',
                    type: 'lines',
                    coordinateSystem: 'geo',
                    zlevel: 2,
                    large: true,
                    // effect: {  //线路静态效果
                    //     show: true,
                    //     constantSpeed: 30,
                    //     symbol: 'arrow', //ECharts 提供的标记类型包括 'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow'
                    //     symbolSize: 0,
                    //     trailLength: 0,
                    // },

                    // lineStyle: {
                    //     normal: {
                    //         color: '#2BF6F8',
                    //         // color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{ //线路渐变
                    //         //         offset: 0, color: '#58B3CC'
                    //         //     }, {
                    //         //         offset: 1, color: '#F58158'
                    //         //     }], false),
                    //         width: 2,
                    //         opacity: 1.0,
                    //         curveness: 0.15
                    //     }
                    // },

                    effect: { //线路动态效果
                        show: true,
                        period: 1,
                        trailLength: 0.7,
                        color: '#54F5F5',
                        symbolSize: 4
                    },
                    lineStyle: {
                        normal: {
                            width: '',
                            // color: '#a6c84c',
                            curveness: 0.2
                        }
                    },
                    data: moveLine.normal
                }
            ]
        };
        $('.sxc-box').hide();
        $('.tzc-box').hide();
        $('.zjn-box').hide();
    }
    myChart.setOption(
        option
    );
    myChart.resize();
};
